/**
 * Created by tyler on 12/22/15.
 */
var currentSong = null;
var current_total_songs = null;
getPlaylists();


function getPlaylists(){
    $.post("php/Playlist.php", {info: "getPlaylists"}, function(data){
        var json = data,
            obj = JSON.parse(json);

        $(".playlists").html("");
        obj.forEach(function(element){
           $(".playlists").html($(".playlists").html() + getPlaylistHtml(element))
        });

        $(".playlist[id='1']").addClass("active");
        hasNewSong(1);

        getSongs(1);
    });
}
function getSongs(playlist){
    console.log("changed songs");
    $.post("php/Song.php", {info: "getSongs", id:playlist}, function(data){
        var json = data,
            obj = JSON.parse(json);

            $(".songs").html("");

        obj.forEach(function(element){
            $(".songs").html($(".songs").html() + getSongHtml(element));
        });


    });

}
function getPlaylistHtml(data){
    var template = ".playlistExample";
    var id = data["id"];
    var name = data["name"];
    var totalSongs = data["total_songs"];

    $(template + " .playlist").attr("id", id);
    $(template + " .playlist").attr("name", name);
    $(template + " .playlist").attr("total_songs", totalSongs);

    $(template+" #id").html(id);
    $(template+" #name").html(name);
    $(template+" #total_songs").html(totalSongs);
    return $(template).html();
}
function getSongHtml(data){

    var template = ".songExample";
    var id = data["id"];
    var name = data["name"];
    var type = data["total_songs"];
    var ref = data["ref"];

    $(template + " .song").attr("id", id);
    $(template + " .song").attr("name", name);
    $(template + " .song").attr("type", type);
    $(template + " .song").attr("ref", ref);

    $(template+" #id").html(id);
    $(template+" #name").html(name);
    $(template+" #type").html(type);
    $(template+" #ref").html(ref);
    return $(template).html();

    return "";
}

$(document).on("click", ".playlist", function(event){selectPlaylist(event);});
function selectPlaylist(event){
    current_total_songs = $(event.target).attr('total_songs');
    getSongs($(event.target).attr('id'));
}

$(document).on("click", ".song", function(event){selectSong(event)});
function selectSong(event){
    playSong($(event.target).attr("ref"), $(event.target).attr("id"));
}
function playSong(yt_id, songId){
    player.loadVideoById(yt_id);
    currentSong = songId;
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED && !done) {
        nextSong();
    }
}

function nextSong(){
    var current = $(".song[id='"+currentSong+"']");
    current = $(current);
    var next = current.next(".song");
    next = $(next);

    if(next.html() != null)
        playSong(next.attr("ref"), next.attr("id"));
    else
        playSong($($(".songs").children()[0]).attr("ref"),$($(".songs").children()[0]).attr("id"))

}

function hasNewSong(playlist_id){
    $.post("php/Playlist.php", {info: "getPlaylist", id:playlist_id}, function(data){
        if(current_total_songs != data){
            //alert($(".playlist[id='"+playlist_id+"'").attr("id"));
            getSongs($(".playlists [id='"+playlist_id+"'").attr("id"));
            $(".playlists [id='"+playlist_id+"']").attr("total_songs", data);
            $(".playlists [id='"+playlist_id+"'] #total_songs").html(data);
            current_total_songs = data;
        }
    });

    setTimeout(function(){hasNewSong($(".playlist.active").attr("id"))}, 10000);
}