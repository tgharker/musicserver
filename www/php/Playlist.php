<?php
/*
if(!isset($_REQUEST['info'])){
    echo "Need to declare name of function.";
    return;
}
*/


require 'Database.php';


if($_REQUEST['info'] == 'getPlaylists') getPlaylists();
if($_REQUEST['info'] == 'getPlaylist') getPlaylist($_REQUEST['id']);

function getPlaylists(){
    $database = new Database();
    $sql = "SELECT id, name, total_songs FROM playlist";

    $results = $database->conn->query($sql);
    $data = array();

    while($row = $results->fetch_assoc()) {
        $data[sizeof($data)] = $row;
    }
    echo json_encode ($data);
}
function getPlaylist(){
    $database = new Database();
    $sql = "SELECT total_songs FROM playlist";

    $results = $database->conn->query($sql);
    $row = $results->fetch_assoc();
    echo $row['total_songs'];
}
